package com.twuc.webApp;

import com.twuc.webApp.entity.AnotherDependent;
import com.twuc.webApp.entity.Dependent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithAutowiredMethod {

    private Dependent dependent;

    private AnotherDependent anotherDependent;

    public Dependent getDependent() {
        return dependent;
    }

    public WithAutowiredMethod(Dependent dependent) {
        this.dependent = dependent;
    }

    public AnotherDependent getAnotherDependent() {
        return this.anotherDependent;
    }

    @Autowired
    private void initialize(AnotherDependent anotherDependent) {
        this.anotherDependent = anotherDependent;
    }
}
