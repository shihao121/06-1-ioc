package com.twuc.webApp;

import com.twuc.webApp.entity.SimpleDependence;
import com.twuc.webApp.entity.SimpleObject;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleObjectFactory {

    @Bean
    public SimpleObject getObject() {
        SimpleDependence simpleDependence = new SimpleDependence();
        simpleDependence.setName("O_o");
        return new SimpleObject(simpleDependence);
    }
}
