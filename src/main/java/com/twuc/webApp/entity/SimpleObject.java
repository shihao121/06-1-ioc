package com.twuc.webApp.entity;

import org.springframework.stereotype.Component;

public class SimpleObject implements SimpleInterface{

    private SimpleDependence simpleDependence;

    public SimpleObject(SimpleDependence simpleDependence) {
        this.simpleDependence = simpleDependence;
    }

    @Override
    public SimpleDependence getSimpleDependence() {
        return simpleDependence;
    }
}
