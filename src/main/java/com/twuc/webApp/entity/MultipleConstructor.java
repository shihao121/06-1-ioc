package com.twuc.webApp.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    private Dependent dependent;

    private String constructor;

    public Dependent getDependent() {
        return dependent;
    }

    public String getConstructor() {
        return constructor;
    }

    @Autowired
    public MultipleConstructor(Dependent dependent) {
        this.dependent = dependent;
    }

    public MultipleConstructor(String constructor) {
        this.constructor = constructor;
    }
}
