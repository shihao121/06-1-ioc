package com.twuc.webApp.entity;

import org.springframework.stereotype.Component;

@Component
public class Person {

    private Phone phone;

    public Person(Phone phone) {
        this.phone = phone;
    }

    public Phone getPhone() {
        return phone;
    }
}
