package com.twuc.webApp.simplest;

import com.twuc.webApp.Student;
import com.twuc.webApp.WithAutowiredMethod;
import com.twuc.webApp.entity.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

class ObjectWithoutDependency {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    private void setContext() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_create_bean_without_dependence() {
        Person person = context.getBean(Person.class);
        assertSame(Person.class, person.getClass());
    }

    @Test
    void should_create_bean_with_dependence() {
        Person person = context.getBean(Person.class);
        assertSame(person.getClass(), Person.class);
        assertSame(person.getPhone().getClass(), Phone.class);
    }

    @Test
    void should_create_bean_fail_when_not_scan_package() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp.entity");
        assertThrows(NoSuchBeanDefinitionException.class, () -> {
            context.getBean(Student.class);
        });
    }

    @Test
    void should_create_interface_impl_bean_when_get_bean_by_interface() {
        Interface interfaceImpl = context.getBean(Interface.class);
        assertSame(interfaceImpl.getClass(), InterfaceImpl.class);
    }

    @Test
    void should_initialize_bean_with_filed_when_create_bean() {
        SimpleInterface simple = context.getBean(SimpleInterface.class);
        assertSame(simple.getClass(), SimpleObject.class);
        assertSame(simple.getSimpleDependence().getClass(), SimpleDependence.class);
        assertEquals(simple.getSimpleDependence().getName, "O_o");
    }

    @Test
    void should_create_bean_with_specify_constructor() {
        MultipleConstructor multipleConstructor = context.getBean(MultipleConstructor.class);
        assertSame(MultipleConstructor.class, multipleConstructor.getClass());
        assertSame(Dependent.class, multipleConstructor.getDependent().getClass());
    }

    @Test
    void should_create_bean_with_all_filed_when_have_constructor_and_autowired() {
        WithAutowiredMethod withAutowiredMethod = context.getBean(WithAutowiredMethod.class);
        assertNotNull(withAutowiredMethod);
        assertNotNull(withAutowiredMethod.getDependent());
        assertNotNull(withAutowiredMethod.getAnotherDependent());
    }
}
